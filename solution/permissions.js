/*

	SOLUTION

	You cannot just copy and paste this because
	the bucket name will need to be your bucket name

	If you run it "as is" it will not work!

	You must replaace <FMI> with your bucket name

	e.g

	2019-03-20-ricky-catlostandfoundwebsite

	Keeo the quotes in there below, and literally just 
	replace the characters <FMI>


*/

var 
    AWS = require("aws-sdk"),
    S3API = new AWS.S3({
        apiVersion: "2006-03-01"
    }),
    PUBLIC_POLICY_STR = JSON.stringify(require("./public_policy.json"));

(function addPublicBucketPolicy(){
    var 
        params = {
            Bucket: "<FMI>",
            Policy: PUBLIC_POLICY_STR
            
        };
        S3API.putBucketPolicy(params, function(error, data){
            if(error){
                console.log(error);
                throw error;
            }
            console.log("Bucket Policy Set", data);
        });
})();